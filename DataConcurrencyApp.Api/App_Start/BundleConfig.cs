﻿using System.Web.Optimization;

namespace DataConcurrencyApp.Api
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
                        "~/Scripts/jquery-1.10.2.min.js",
                        "~/Scripts/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                      "~/Scripts/app/main.js",
                      "~/Scripts/app/login.js",
                      "~/Scripts/app/dataManager.js",
                      "~/Scripts/app/registration.js"));
        }
    }
}
