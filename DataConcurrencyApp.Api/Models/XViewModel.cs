﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataConcurrencyApp.Api.Models
{
    public class XViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [StringLength(20, ErrorMessage = "First Name must be between 3 and 20 characters", MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(20, ErrorMessage = "Last Name must be between 3 and 20 characters", MinimumLength = 3)]
        public string LastName { get; set; }

        [Range(0, 120, ErrorMessage = "Invalid age. Must be between 0 and 120 characters")]
        [Required(ErrorMessage = "Age is required")]
        public byte Age { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(40, ErrorMessage = "Address must be between 3 and 40 characters", MinimumLength = 3)]
        public string Address { get; set; }

        public string Version { get; set; }
    }
}