﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConcurrencyApp.Api.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Login is required")]
        [StringLength(10, ErrorMessage = "Password must be between 3 and 10 characters", MinimumLength = 3)]
        public string Login { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(10, ErrorMessage = "Password must be between 3 and 10 characters", MinimumLength = 3)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
