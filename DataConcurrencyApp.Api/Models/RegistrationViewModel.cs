﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataConcurrencyApp.Api.Models
{
    public class RegistrationViewModel
    {
        [Required(ErrorMessage = "Login is required")]
        [StringLength(10, ErrorMessage = "Must be between 3 and 10 characters", MinimumLength = 3)]
        public string Login { get; set; }

        [MinLength(3)]
        [MaxLength(10)]
        [Required(ErrorMessage = "Password is required")]
        [StringLength(10, ErrorMessage = "Must be between 3 and 10 characters", MinimumLength = 3)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(10, ErrorMessage = "Must be between 3 and 10 characters", MinimumLength = 3)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Your password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
