﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataConcurrencyApp.Api.Models
{
    public class YViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(20, ErrorMessage = "First Name must be between 3 and 20 characters", MinimumLength = 3)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(20, ErrorMessage = "Last Name must be between 3 and 20 characters", MinimumLength = 3)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Age is required")]
        [Range(0, 120, ErrorMessage = "Invalid age. Must be between 0 and 120 characters")]
        public int Age { get; set; }
        [Required(ErrorMessage = "Address is required")]
        [StringLength(40, ErrorMessage = "Address must be between 3 and 40 characters", MinimumLength = 3)]
        public string Address { get; set; }
        public bool IsLocked { get; set; }
        public string LockedByLogin { get; set; }
    }
}