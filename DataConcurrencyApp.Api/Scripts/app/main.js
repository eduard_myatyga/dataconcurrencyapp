﻿$(document).ready(function () {
   
    $(AppConstants.User.LogoutId).click(function (event) {
        event.preventDefault();
        Logout();
    });
    window.checkActive();
});

window.dataManager = {
    Hide: function () {
        $(AppConstants.DataManager.Id).css('display', 'none');
    },
    Show: function () {
        $(AppConstants.DataManager.Id).css('display', 'block');
    }
}
window.panels = {
    X: {
        Show: function () {
            $(AppConstants.Panels.X.Id).css('display', 'block');
        },
        Clear: function () {
            $(AppConstants.Panels.X.Id).css('display', 'none');
            $(AppConstants.Tables.X.Id).empty();
        }
    },
    Y: {
        Show: function () {
            $(AppConstants.Panels.Y.Id).css('display', 'block');
        },
        Clear: function () {
            $(AppConstants.Panels.Y.Id).css('display', 'none');
            $(AppConstants.Tables.Y.Id).empty();
        }
    }
}
window.loginForm = {
    Hide: function () {
        $(AppConstants.Forms.Ids.loginForm).css('display', 'none');
    },
    Show: function () {
        $(AppConstants.Forms.Ids.loginForm).css('display', 'block');
    }
}
window.getAuth = function () {
    var fromStorage = JSON.parse(localStorage.getItem(AppConstants.LocalStorage.AuthKey));
    if (fromStorage === undefined || fromStorage === null) {
        return {};
    }
    return { Login: fromStorage.Login, Token: fromStorage.Token }
}
window.removeAuth = function () {
    localStorage.removeItem(AppConstants.LocalStorage.AuthKey);
}
window.setAuth = function (data) {
    var authData = { Login: data.Login, Token: data.Token };
    localStorage.setItem(AppConstants.LocalStorage.AuthKey, JSON.stringify(authData));
}
window.userDisplay = {
    Show: function () {
        var user = getAuth().Login;
        var li = $(AppConstants.User.NameBlockId);
        var container = '<b> <h2> Hello, ' + user + '</h2></b>';
        li.html(container);
        li.css('display', 'block');
        $(AppConstants.User.LogoutId).css('display', 'block');
    },
    Hide: function () {
        var li = $(AppConstants.User.NameBlockId);
        li.empty();
    }
}
window.AppConstants = {
    Notifications:{
        Id: " .formNotification"
    },
    DataManager: {
        Id: ".dataManager"
    },
    Errors: {
        Id: " .formErrors"
    },
    LocalStorage: {
        AuthKey: "auth"
    },
    ApiUrls: {
        Login: 'api/Account/Login',
        Registration: 'api/Account/Registration',
        KeepAlive: 'api/Account/active',
        Logout: 'api/Account/Logout',
        AllX: 'api/data/contact_x/all',
        AllY: 'api/data/contact_y/all',
        UpdateY: 'api/data/contact_y/update',
        UpdateX: 'api/data/contact_x/update',
        InsertX: 'api/data/contact_x/insert',
        InsertY: 'api/data/contact_y/insert',
        DeleteX: function (id) {
            return 'api/data/contact_x/delete/' + id;
        },
        DeleteY: function (id) {
            return 'api/data/contact_y/delete/' + id;
        },
        GetY: function (id) {
            return 'api/data/contact_y/' + id;
        },
        UnlockY: function (id) {
            return 'api/data/contact_y/unlock/' + id;
        },
        GetX: function (id) {
            return 'api/data/contact_x/' + id;
        }
    },
    Forms: {
        Ids: {
            "loginForm": "#loginForm",
            "edit_form_y": "#edit_form_y",
            "add_form_y": "#add_form_y",
            "edit_form_x": "#edit_form_x",
            "add_form_x": "#add_form_x",
            "merge_form_x": "#merge_form_x",
            "register_form": "#register_form"
        }
    },
    User: {
        isActive: false,
        NameBlockId: "#userDetails",
        LogoutId: "#logout"
    },
    Panels: {
        X: {
            Id: "#panel_x"
        },
        Y: {
            Id: "#panel_y"
        }
    },
    Tables: {
        X: {
            Id: "#tableXBlock"
        },
        Y: {
            Id: "#tableYBlock"
        }
    }
}
window.parseModelStateErrors = function (response) {
    var errors = [];
    var data = JSON.parse(response.responseText);
    for (var key in data.ModelState) {
        if (data.ModelState.hasOwnProperty(key)) {
            for (var i = 0; i < data.ModelState[key].length; i++) {
                errors.push(data.ModelState[key][i]);
            }
        }
    }
    return errors;
}
window.checkActive = function () {
    var fromStorage = JSON.parse(localStorage.getItem(AppConstants.LocalStorage.AuthKey));
    if (fromStorage === undefined || fromStorage === null) {
        userDisplay.Hide();
        dataManager.Hide();
        renderLoginForm();
        AppConstants.User.isActive = false;
    }
    else {
        var config = { url: AppConstants.ApiUrls.KeepAlive };
        window.callApi(config, function (data) {
            loginForm.Hide();
            userDisplay.Show();
            dataManager.Show();
            AppConstants.User.isActive = true;
        },
            function (jqXHR, textStatus, error) {
                if (jqXHR.status = 401) {
                    console.log(jqXHR);
                    userDisplay.Hide();
                    dataManager.Hide();
                    renderLoginForm();
                    AppConstants.User.isActive = false;
                }
            });
    }
}
window.callApi = function (config, onSuccess, onError, onBefore, setHeaders) {
    $.ajax({
        url: config.url,
        type: config.type || 'GET',
        data: config.data,
        dataType: 'json',
        success: onSuccess,
        error: onError,
        beforeSend: onBefore || function () { formbuilder.destroyAllForms(); },
        headers: setHeaders || { 'sTok': window.getAuth().Token }
    });
};
function Logout() {
    var config = { url: AppConstants.ApiUrls.Logout, type: 'POST' }
    callApi(config, function (data) {
        if (data) {
            userDisplay.Hide();
            dataManager.Hide();
            panels.X.Clear();
            panels.Y.Clear();
            window.AppConstants.User.isActive = false;
            removeAuth();
            renderLoginForm();
        }
    },
        function (x, y, z) { alert(x + ' ' + y + ' ' + z) });
}
window.formNotification = {
    Show: function (formId, additionalData) {

        var strResult = '';
        $.each(additionalData, function (index, data) {
            strResult += "<div class='alert alert-success'>" + "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
                + '<p>' + data + '</p>' + "</div>";

        });
        var parent = $(formId);
        if (parent.length > 0) {
            $(formId + AppConstants.Notifications.Id).html(strResult);
            $(formId + AppConstants.Notifications.Id).css('display', 'block');
        }
    },
    Hide: function () {
        $(AppConstants.Notifications.Id).empty();
    }
}
window.formErrors = {
    Show: function (data, formId, additionalError) {
        var errors = parseModelStateErrors(data);
        if (additionalError !== undefined) {
            $.each(additionalError, function (index, err) {
                errors.push(err);
            });
        }
        var strResult = '';
        $.each(errors, function (index, error) {
            strResult += "<div class='alert alert-danger'>" + "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
                + '<p>' + error + '</p>' + "</div>";

        });
        var parent = $(formId);
        if (parent.length > 0) {
            $(formId + AppConstants.Errors.Id).html(strResult);
            $(formId + AppConstants.Errors.Id).css('display', 'block');
        }
    },
    Hide: function () {
        $(AppConstants.Errors.Id).empty();
    }

}
window.pastJsonToForm = function (data) {
    if (data != null) {
        Object.keys(data).forEach(function (key) {
            var id = '#' + key;
            $(id).val(data[key]);
        });
    }
}

window.formbuilder = (function () {
    var forms = [];
    return {
        addForm: function (parentContainerId, idForm, visibleFields, hiddenFields, visibleButtons, styles, canRemovedBody) {
            var parentContainer = $(parentContainerId);
            canRemovedBody = canRemovedBody || false;
            if (parentContainer.length <= 0) {
                return;
            }
            var cssStyle = styles || ' ';
            var mainContainer = "<form id='" + idForm.replace('#', '') + "' class='form-horizontal " + cssStyle + "' >";
            var errorContainer = "<div class='formErrors'></div>";
            var notificationContainer = "<div class='formNotification'></div>";
            var htmlContent = null;
            var htmlContentWithoutBody = '';
            var divFormGroupsVisible = '';
            var divFormGroupsHiden = '';
            var divButtonsGroup = '';
            if (visibleFields != null) {
                Object.keys(visibleFields).forEach(function (key) {
                    var name = key;
                    var title = visibleFields[key].labelTitle || key;
                    var inputType = visibleFields[key].inputType || 'text';
                    divFormGroupsVisible += "<div class='form-group'>" + "<label class='col-sm-3 control-label' for='"
                        + name + "' control-label>" + title + "</label>"
                        + "<div class='col-sm-6'>" + "<input class='form-control' name='" + name + "' id='" + name + "'" + " type='" + inputType + "'/>" + "</div><div class='dx dx_" + name + "'></div></div> ";

                });
            }
            if (hiddenFields != null) {
                Object.keys(hiddenFields).forEach(function (key) {
                    var name = key;
                    divFormGroupsHiden += "<input type='hidden' name='" + name + "' id='" + name + "'/>";
                });
            }
            if (visibleButtons != null) {
                Object.keys(visibleButtons).forEach(function (key) {
                    var name = key;
                    var btnText = visibleButtons[key].btnText;
                    var fnName = visibleButtons[key].FnName;
                    divButtonsGroup += "<div class='col-sm-12 col-sm-push-3'>" + "<button class='btn btn-primary' id='" + name + "' onclick='" + fnName + "'>" + btnText + "</button></div></br>";
                });
            }
            if (canRemovedBody === true)
                htmlContentWithoutBody = mainContainer + errorContainer + notificationContainer + divFormGroupsHiden + "</form>";

            htmlContent = mainContainer + errorContainer + notificationContainer + divFormGroupsHiden + divFormGroupsVisible + divButtonsGroup + "</form>";

            var form = { "id": idForm, "html": htmlContent, "parent": parentContainer, "partial": htmlContentWithoutBody, isActive : false };
            forms.push(form);
        },
        renderForm: function (formId) {
            for (var i = 0; i < forms.length; i++) {
                if ("id" in forms[i] && "html" in forms[i]) {
                    if (formId === forms[i].id) {
                        forms[i].isActive = true;
                        $(forms[i].parent).html(forms[i].html);
                        $(formId).css('display', 'block');
                        break;
                    }
                }
            }
        },
        renderFormPartial: function (formId, isReCreation) {
            for (var i = 0; i < forms.length; i++) {
                if ("id" in forms[i] && "partial" in forms[i]) {
                    if (formId === forms[i].id && forms[i].partial != null) {
                        forms[i].isActive = true;
                        if (isReCreation === true) $(forms[i].parent).empty();
                        $(forms[i].parent).html(forms[i].partial);
                        $(formId).css('display', 'block');
                        break;
                    }
                }
            }
        },
        destroyForm: function (formId) {
            for (var i = 0; i < forms.length; i++) {
                if ("id" in forms[i] && "html" in forms[i]) {
                    if (formId === forms[i].id) {
                        $(forms[i].parent).empty();
                        break;
                    }
                }
            }
        },
        isExist: function (formId) {
            for (var i = 0; i < forms.length; i++) {
                if ("id" in forms[i] && "html" in forms[i]) {
                    if (formId === forms[i].id) {
                        return true;
                    }
                }
            }
            return false;
        },
        showDifferentValueFields: function (formId, fields, css) {
            for (var i = 0; i < forms.length; i++) {
                if ("id" in forms[i] && "html" in forms[i]) {
                    if (formId === forms[i].id) {
                        $.each(fields, function (key, value) {
                            var input = $(formId + ' #' + key);
                            if (input.val() != value) {
                                input.addClass(css || 'error-field');
                                var div = $(formId + ' .dx_' + key);
                                div.html(value || '');
                            }
                        });
                        break;
                    }
                }
            }
        },
        destroyAllForms: function () {
            for (var i = 0; i < forms.length; i++) {
                if(forms[i].isActive = true)
                $(forms[i].parent).empty();
            }
        }
    }
}());
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

