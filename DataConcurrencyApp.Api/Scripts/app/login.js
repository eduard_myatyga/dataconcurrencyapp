﻿function signIn() {
    event.preventDefault();
    var config = {
        url: AppConstants.ApiUrls.Login,
        type: 'POST',
        data: $(AppConstants.Forms.Ids.loginForm).serializeArray()
    }
    callApi(config, loginSuccess, loginOnError);
}


function renderLoginForm() {
    event.preventDefault();
    formbuilder.destroyAllForms();
    if (formbuilder.isExist(AppConstants.Forms.Ids.loginForm) === false) {
        var visibleFields = {
            "Login": { "labelTitle": "Login" },
            "Password": { "labelTitle": "Password", "inputType": "password" }
        };

        var visibleButtons = {
            "loginBtn": { "btnText": "Login", "FnName": "signIn();" },
            "registerBtn": { "btnText": "Registration new", "FnName": "renderSignUpForm();" }
        };
        formbuilder.addForm("#loginForm_container", AppConstants.Forms.Ids.loginForm,
            visibleFields, {}, visibleButtons, '', true);
    };
    formbuilder.renderForm(AppConstants.Forms.Ids.loginForm);
}
function loginSuccess(data) {
    if(data.Login !== undefined || data.Token !== undefined)
    {
        window.setAuth(data);
        formbuilder.destroyForm(AppConstants.Forms.Ids.loginForm);
        userDisplay.Show();
        dataManager.Show();
        window.AppConstants.User.isActive = true;
    }
}
function loginOnError(data) {
    formbuilder.renderForm(AppConstants.Forms.Ids.loginForm)
    window.formErrors.Show(data, AppConstants.Forms.Ids.loginForm);
}