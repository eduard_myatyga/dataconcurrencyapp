﻿function signUp() {
    event.preventDefault();
    var config = {
        url: AppConstants.ApiUrls.Registration,
        type: 'POST',
        data: $(AppConstants.Forms.Ids.register_form).serializeArray()
    }
    callApi(config, registerSuccess, registerOnError);
}
function renderSignUpForm() {
    event.preventDefault();
    formbuilder.destroyAllForms();
    if (formbuilder.isExist(AppConstants.Forms.Ids.register_form) === false) {
        var visibleFields = {
            "Login": { "labelTitle": "Login" },
            "Password": { "labelTitle": "Password", "inputType": "password" },
            "ConfirmPassword": { "labelTitle": "Confirm Password", "inputType": "password" }
        };

        var visibleButtons = {
            "registerBtn": { "btnText": "Registration", "FnName": "signUp();" },
            "renderLoginBtn": { "btnText": "Back to Login", "FnName": "renderLoginForm();" }
        };
        formbuilder.addForm("#loginForm_container", AppConstants.Forms.Ids.register_form,
            visibleFields, {}, visibleButtons, '', true);
    };
    formbuilder.renderForm(AppConstants.Forms.Ids.register_form);
}
function registerSuccess(data) {
    if (data.Login !== undefined) {
        formbuilder.renderForm(AppConstants.Forms.Ids.loginForm);
        window.formNotification.Show(AppConstants.Forms.Ids.loginForm, ["Account was successfully created. Try login."]);
    }
}
function registerOnError(data) {
    formbuilder.renderForm(AppConstants.Forms.Ids.register_form);
    window.formErrors.Show(data, AppConstants.Forms.Ids.register_form);
}