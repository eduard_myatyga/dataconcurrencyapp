﻿$(document).ready(function () {
    $("#xbtn").click(function (event) {
        event.preventDefault();
        if (AppConstants.User.isActive) {
            getAllX();
            panels.Y.Clear();
            panels.X.Show();
        }

    });
    $("#ybtn").click(function (event) {
        event.preventDefault();
        if (AppConstants.User.isActive) {
            getAllY();
            panels.X.Clear();
            panels.Y.Show();
        }

    });

});
function yCancelBtnClick() {
    event.preventDefault();
    var id = parseInt($("#Id").val());
    var config = { url: AppConstants.ApiUrls.UnlockY(id), type: 'GET' }
    callApi(config);
}
function yEditBtnClick() {
    event.preventDefault();
    if (AppConstants.User.isActive) {
        UpdateY();
    }
}
function xAddBtnClick() {
    event.preventDefault();
    if (AppConstants.User.isActive) {
        AddX();
    }
}
function yAddBtnClick() {
    event.preventDefault();
    if (AppConstants.User.isActive) {
        AddY();
    }
}
function xEditBtnClick() {
    event.preventDefault();
    if (AppConstants.User.isActive) {
        UpdateX();
    }
}
function getAllX() {
    var config = { url: AppConstants.ApiUrls.AllX, type: 'GET' }
    callApi(config, onSuccessX, onError);
}
function onSuccessX(data) {
    $(AppConstants.Tables.X.Id).empty();
    var strResult = "<table class='table table-striped'><th>Id</th><th>Row Version</th><th>First Name</th><th>Last Name</th><th>Address</th><th>Age</th>";
    $.each(data, function (index, entity) {
        strResult += "<tr><td>" + entity.Id + "</td><td> " + entity.Version + "</td><td>" +
        entity.FirstName + "</td><td>" + entity.LastName + "</td><td>" + entity.Address + "</td><td>" + entity.Age +
        "</td><td><a id='editItem' data-action='X-edit' data-item='" + entity.Id + "' onclick='TryGetItem(this);'>Edit</a></td>"+
            "<td><a id='deleteItem' data-action='X-delete' data-item='" + entity.Id + "' onclick='TryGetItem(this);'>Delete</a></td></tr>";
    });
    strResult += "</table>";
    $(AppConstants.Tables.X.Id).html(strResult);
}
function onError(jqXhr, textStatus, error) {
    if (jqXhr.status === 401) {
        panels.X.Clear();
        panels.Y.Clear();
        userDisplay.Hide();
        dataManager.Hide();
        formbuilder.renderForm(AppConstants.Forms.Ids.loginForm);
    }
}
function getAllY() {
    var config = { url: AppConstants.ApiUrls.AllY, type: 'GET' };
    callApi(config, onSuccessY, onError);
}
function onSuccessY(data) {
    $(AppConstants.Tables.Y.Id).empty();
    var strResult = "<table class='table table-striped'><th>Id</th><th>Locked</th><th>LockedBY</th><th>First Name</th><th>Last Name</th><th>Address</th><th>Age</th>";
    $.each(data, function (index, entity) {
        strResult += "<tr><td>" + entity.Id + "</td><td> " + entity.IsLocked + "</td><td>" + entity.LockedByLogin + "</td><td>" +
        entity.FirstName + "</td><td>" + entity.LastName + "</td><td>" + entity.Address + "</td><td>" + entity.Age +
        "</td><td><a id='editItem' data-action='Y-edit' data-item='" + entity.Id + "' onclick='TryGetItem(this);'>Edit</a></td>"+
            "<td><a id='deleteItem' data-action='Y-delete' data-item='" + entity.Id + "' onclick='TryGetItem(this);'>Delete</a></td></tr>";
    });
    strResult += "</table>";
    $(AppConstants.Tables.Y.Id).html(strResult);
}
function TryGetItem(el) {
    var id = $(el).attr('data-item');
    var action = $(el).attr('data-action');
    switch (action) {
        case 'Y-edit':
            {
                renderYEditForm();
                GetY(id);
            } break;
        case 'X-edit':
            {
                renderXEditForm();
                GetX(id);
            } break;
        case 'Y-add': {
            renderYAddForm();
        } break;
        case 'X-add': {
            renderXAddForm();
        } break;
        case 'X-delete': {
            var r = confirm("Do you want delete this entity X?");
            if(r === true)
                DeleteX(id);
        } break;
        case 'Y-delete': {
            var r = confirm("Do you want delete this entity Y?");
            if (r === true)
                DeleteY(id);
        } break;
        default: alert('Error for EditItem with data-entity attr: ' + entity);
    }
}
function GetY(id) {
    var config = { url: AppConstants.ApiUrls.GetY(id) };
    callApi(config, function (data) { formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_y); showEntity(data) }, function (data) {
        window.formbuilder.renderFormPartial(AppConstants.Forms.Ids.edit_form_y, true);
        window.formErrors.Show(data, AppConstants.Forms.Ids.edit_form_y);
    });
}
function GetX(id) {
    var config = { url: AppConstants.ApiUrls.GetX(id) };
    callApi(config, function (data) { formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_x); showEntity(data) }, function (data) {
        window.formbuilder.renderFormPartial(AppConstants.Forms.Ids.edit_form_x, true);
        window.formErrors.Show(data, AppConstants.Forms.Ids.edit_form_x);
    });
}

function renderXAddForm() {
    if (formbuilder.isExist(AppConstants.Forms.Ids.add_form_x) === false) {
        var visibleFields = {
            "FirstName": { "labelTitle": "First Name" },
            "LastName": { "labelTitle": "Last Name" },
            "Address": { "labelTitle": "Address" },
            "Age": { "labelTitle": "Age", "inputType": "number" }
        };

        var visibleButtons = {
            "xAddBtn": { "btnText": "Add", "FnName": "xAddBtnClick();" }
        };
        formbuilder.addForm("#edit_container_x", AppConstants.Forms.Ids.add_form_x,
            visibleFields, null, visibleButtons, '', true);
    };
    formbuilder.renderForm(AppConstants.Forms.Ids.add_form_x);
}
function renderYAddForm() {
    if (formbuilder.isExist(AppConstants.Forms.Ids.add_form_y) === false) {
        var visibleFields = {
            "FirstName": { "labelTitle": "First Name" },
            "LastName": { "labelTitle": "Last Name" },
            "Address": { "labelTitle": "Address" },
            "Age": { "labelTitle": "Age", "inputType": "number" }
        };

        var visibleButtons = {
            "yAddBtn": { "btnText": "Add", "FnName": "yAddBtnClick();" }
        };
        formbuilder.addForm("#edit_container_x", AppConstants.Forms.Ids.add_form_y,
            visibleFields, null, visibleButtons, '', true);
    };
    formbuilder.renderForm(AppConstants.Forms.Ids.add_form_y);
}
function renderXEditForm() {
    if (formbuilder.isExist(AppConstants.Forms.Ids.edit_form_x) === false) {
        var visibleFields = {
            "FirstName": { "labelTitle": "First Name" },
            "LastName": { "labelTitle": "Last Name" },
            "Address": { "labelTitle": "Address" },
            "Age": { "labelTitle": "Age", "inputType": "number" }
        };
        var hiddenFields = {
            "Id": "Id",
            "Version": "Version"
        }

        var visibleButtons = {
            "xEditBtn": { "btnText": "Update", "FnName": "xEditBtnClick();" }
        };
        var hiddenButtons = {
            "xResolveBtn": { "btnText": "Update", "FnName": "xEditBtnClick();" }
        }
        formbuilder.addForm("#edit_container_x", AppConstants.Forms.Ids.edit_form_x,
            visibleFields, hiddenFields, visibleButtons, '', true);
    };
    formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_x);
}
function renderYEditForm() {
    if (formbuilder.isExist(AppConstants.Forms.Ids.edit_form_y) === false) {
        var visibleFields = {
            "FirstName": { "labelTitle": "First Name" },
            "LastName": { "labelTitle": "Last Name" },
            "Address": { "labelTitle": "Address" },
            "Age": { "labelTitle": "Age", "inputType": "number" }
        };
        var hiddenFields = {
            "Id": "Id"
        }

        var visibleButtons = {
            "yEditBtn": { "btnText": "Update", "FnName": "yEditBtnClick();" },
            "yCancelBtn": { "btnText": "Cancel", "FnName": "yCancelBtnClick();" }
        };
        formbuilder.addForm("#edit_container_y", AppConstants.Forms.Ids.edit_form_y,
            visibleFields, hiddenFields, visibleButtons, '', true);
    };
    formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_y);
}

function showEntity(data) {
    pastJsonToForm(data);
}

function UpdateY() {
    var formData = $(AppConstants.Forms.Ids.edit_form_y).serializeObject();
    var config = {
        url: AppConstants.ApiUrls.UpdateY,
        type: 'POST',
        data: formData
    }
    callApi(config, function (data) {
        getAllY();
        window.formbuilder.renderFormPartial(AppConstants.Forms.Ids.edit_form_y, true);
        window.formNotification.Show(AppConstants.Forms.Ids.edit_form_y, ["Entity Y was successfully updated"]);
    }, function (errors) {
        window.formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_y);
        window.formErrors.Show(errors, AppConstants.Forms.Ids.edit_form_y);
        showEntity(formData);
    }, function () { });
}
function UpdateX() {
    var formData = $(AppConstants.Forms.Ids.edit_form_x).serializeObject();
    var config = {
        url: AppConstants.ApiUrls.UpdateX,
        type: 'POST',
        data: formData
    }
    callApi(config, function () {
        getAllX();
        window.formbuilder.renderFormPartial(AppConstants.Forms.Ids.edit_form_x, true);
        window.formNotification.Show(AppConstants.Forms.Ids.edit_form_x, ["Entity X was successfully updated"]);
    }, function (response) {
        if (response.status === 409) {
            renderConflictFormResolver(response, formData)
            return;
        } else
        {
            window.formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_x);
            window.formErrors.Show(response, AppConstants.Forms.Ids.edit_form_x);
            showEntity(formData);
        }
    });
}

function renderConflictFormResolver(response, formData) {
    if (formbuilder.isExist(AppConstants.Forms.Ids.merge_form_x) === false) {
        var visibleFields = {
            "FirstName": { "labelTitle": "First Name" },
            "LastName": { "labelTitle": "First Last" },
            "Address": { "labelTitle": "Address" },
            "Age": { "labelTitle": "Age", "inputType": "number" }
        };
        var hiddenFields = {
            "Id": "Id",
            "Version": "Version"
        }

        var visibleButtons = {
            "mergeMine": { "btnText": "Merge mine into Db", "FnName": "mergeMineBtn(" + formData.Id + ");" },
            "mergeTheir": { "btnText": "Merge Db into mine", "FnName": "mergeTheirBtn(" + formData.Id + ");" }
        };
        formbuilder.addForm("#merge_container_x", AppConstants.Forms.Ids.merge_form_x,
            visibleFields, hiddenFields, visibleButtons, '', true);
    };
    
    window.callApi({ url: AppConstants.ApiUrls.GetX(formData.Id) }, function (data) {
        formbuilder.renderForm(AppConstants.Forms.Ids.merge_form_x);
        showEntity(formData);
        var additionalError = ["Your version: [" + formData.Version + "] Db version: [" + data.Version + "]"];
        window.formErrors.Show(response, AppConstants.Forms.Ids.merge_form_x, additionalError);
        formbuilder.showDifferentValueFields(AppConstants.Forms.Ids.merge_form_x, data);
    }, function (data) {
        window.formErrors.Show(data, AppConstants.Forms.Ids.merge_form_x);
    });
    
}
function mergeMineBtn(entityId)
{
    event.preventDefault();
    var formData = $(AppConstants.Forms.Ids.merge_form_x).serializeObject();
    formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_x);
    window.callApi({ url: AppConstants.ApiUrls.GetX(entityId) }, function (data) {
        //Merge mine into db version
        var dbVersion = data.Version || '';
        var resultOfMerge = $.extend({}, data, formData);
        resultOfMerge.Version = dbVersion;
        showEntity(resultOfMerge);
        window.formNotification.Show(AppConstants.Forms.Ids.edit_form_x, ["Merge mine  was successfully. Save your changes"]);
    }, function (data) {
        window.formErrors.Show(data, AppConstants.Forms.Ids.edit_form_x);
    }, function () { formbuilder.destroyForm(AppConstants.Forms.Ids.merge_form_x) });
}
function mergeTheirBtn(entityId) {
    event.preventDefault();
    formbuilder.renderForm(AppConstants.Forms.Ids.edit_form_x);
    window.callApi({ url: AppConstants.ApiUrls.GetX(entityId) }, function (data) {
        //Merge db into mine version
        window.formNotification.Show(AppConstants.Forms.Ids.edit_form_x, ["Merge from db was successfully. Save your changes"]);
        showEntity(data);
    }, function (data) {
        window.formErrors.Show(data, AppConstants.Forms.Ids.edit_form_x);
    }, function () { formbuilder.destroyForm(AppConstants.Forms.Ids.merge_form_x) });
}
function AddX()
{
    var formData = $(AppConstants.Forms.Ids.add_form_x).serializeObject();
    var config = {
        url: AppConstants.ApiUrls.InsertX,
        type: 'POST',
        data: formData
    }
    callApi(config, function () {
        getAllX();
        window.formbuilder.renderFormPartial(AppConstants.Forms.Ids.add_form_x,true);
        window.formNotification.Show(AppConstants.Forms.Ids.add_form_x, ["Entity X was was successfully created"]);
    }, function (response) {
        window.formbuilder.renderForm(AppConstants.Forms.Ids.add_form_x);
        window.formErrors.Show(response, AppConstants.Forms.Ids.add_form_x);
        showEntity(formData);
    });
}
function AddY()
{
    var formData = $(AppConstants.Forms.Ids.add_form_y).serializeObject();
    var config = {
        url: AppConstants.ApiUrls.InsertY,
        type: 'POST',
        data: formData
    }
    callApi(config, function () {
        getAllY();
        window.formbuilder.renderFormPartial(AppConstants.Forms.Ids.add_form_y, true);
        window.formNotification.Show(AppConstants.Forms.Ids.add_form_y, ["Entity Y was successfully created"]);
    }, function (response) {
        window.formbuilder.renderForm(AppConstants.Forms.Ids.add_form_y);
        window.formErrors.Show(response, AppConstants.Forms.Ids.add_form_y);
        showEntity(formData);
    });
}
function DeleteX(id)
{
    var config = { url: AppConstants.ApiUrls.DeleteX(id), type: 'POST' }
    callApi(config, function(data) {
        getAllX();
        alert("Entity X was successfully deleted");
    }, failOnDelete);
}
function DeleteY(id) {
    var config = { url: AppConstants.ApiUrls.DeleteY(id), type: 'POST' }
    callApi(config, function(data) {
        getAllY();
        alert("Entity Y was successfully deleted");
    }, failOnDelete);
}
function failOnDelete(response)
{
    var errors = parseModelStateErrors(response);
    var message = '';
    $.each(errors, function (index, data) {
        message += data;
    });
    alert(message);
}