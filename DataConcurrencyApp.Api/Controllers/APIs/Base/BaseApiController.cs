﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using DataConcurrencyApp.Api.Utils;
using DataConcurrencyApp.DAL.Interfaces;
using DataConcurrencyApp.DAL.Repositories;
using DataConcurrencyApp.DAL.Utils;

namespace DataConcurrencyApp.Api.Controllers.APIs.Base
{
    public class BaseApiController : ApiController
    {
        public delegate void ControllerAuthVoidExecutor(Authorizer request);
        public delegate T ControllerAuthReturnExecutor<T>(Authorizer request);

        public string ConnectionStr { get; private set; }
        protected IUserRepository UserRepository { get; private set; }
        protected IContactXRepository ContactXRepository { get; private set; }
        protected IContactYRepository ContactYRepository { get; private set; }

        public BaseApiController()
        {
            ConnectionStr = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
            UserRepository = new UserRepository(ConnectionStr);
            ContactXRepository = new ContactXRepository(ConnectionStr);
            ContactYRepository = new ContactYRepository(ConnectionStr);
        }
        public HttpResponseMessage PerformVoidRequest(HttpRequestMessage request, ControllerAuthVoidExecutor executor, MethodBase method)
        {
            var auth = new Authorizer(request);
            if (!auth.IsAuthorized())
                return Request.CreateResponse(auth.GetHttpErrorCode());
            try
            {
                executor(auth);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (CustomException e)
            {
                var error = new BadRequestResponse();
                error.ModelState.Add(e.GetCode(), new string[] { e.Message });

                return Request.CreateResponse(e.GetCode(), error);
            }
            catch (Exception e)
            {
                var error = new BadRequestResponse();
                error.ModelState.Add(HttpStatusCode.InternalServerError, new string[] { e.Message });

                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }
        public HttpResponseMessage PerformReturnRequest<Y>(HttpRequestMessage request, ControllerAuthReturnExecutor<Y> executor, MethodBase method)
        {
            var auth = new Authorizer(request);
            if (!auth.IsAuthorized())
                return Request.CreateResponse(auth.GetHttpErrorCode());
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, executor(auth));
            }
            catch (CustomException e)
            {
                var error = new BadRequestResponse();
                error.ModelState.Add(e.GetCode(), new string[] { e.Message });
                
                return Request.CreateResponse(e.GetCode(), error);
            }
            catch (Exception e)
            {
                var error = new BadRequestResponse();
                error.ModelState.Add(HttpStatusCode.InternalServerError, new string[] { e.Message });

                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }
        }
    }
}
