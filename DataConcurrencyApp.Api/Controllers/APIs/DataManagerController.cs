﻿using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using DataConcurrencyApp.Api.Controllers.APIs.Base;
using DataConcurrencyApp.Api.Filters;
using DataConcurrencyApp.Api.Models;
using DataConcurrencyApp.Api.Utils;

namespace DataConcurrencyApp.Api.Controllers.APIs
{
    [RoutePrefix("api/data")]
    public class DataManagerController : BaseApiController
    {
        [Route("contact_x/all")]
        [HttpGet]
        public HttpResponseMessage GetAllX()
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactXRepository.GetAll();
                  return contact.Select(x=> x.ConvertToViewModelX());
              },
              MethodBase.GetCurrentMethod());
        }

        [Route("contact_x/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetX([FromUri]int id)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactXRepository.GetSingle(id);
                  return contact;
              },
              MethodBase.GetCurrentMethod());
        }
        [Route("contact_x/insert")]
        [HttpPost]
        [ValidateModel]
        public HttpResponseMessage InserX([FromBody]XViewModel model)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactXRepository.Insert(model.ConvertToDomainModelX());
                  return contact.ConvertToViewModelX();
              },
              MethodBase.GetCurrentMethod());
        }

        [Route("contact_x/update")]
        [HttpPost]
        [ValidateModel]
        public HttpResponseMessage UpdateX([FromBody]XViewModel model)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactXRepository.Update(model.ConvertToDomainModelX());
                  return contact.ConvertToViewModelX();
              },
              MethodBase.GetCurrentMethod());
        }

        [Route("contact_y/all")]
        [HttpGet]
        public HttpResponseMessage GetAllY()
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactYRepository.GetAll();
                  return contact.Select(x=>x.ConvertToViewModelY());
              },
              MethodBase.GetCurrentMethod());
        }

        [Route("contact_y/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetY([FromUri]int id)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactYRepository.GetSingle(id, auth.GetUserId(), true);
                  return contact;
              },
              MethodBase.GetCurrentMethod());
        }
        [Route("contact_y/unlock/{id:int}")]
        [HttpGet]
        public HttpResponseMessage UnlockY([FromUri]int id)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactYRepository.GetSingle(id, auth.GetUserId(), false);
                  return contact;
              },
              MethodBase.GetCurrentMethod());
        }
        [Route("contact_y/insert")]
        [HttpPost]
        [ValidateModel]
        public HttpResponseMessage InsertY([FromBody]YViewModel model)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactYRepository.Insert(model.ConvertToDomainModelY());
                  return contact.ConvertToViewModelY();
              },
              MethodBase.GetCurrentMethod());
        }

        [Route("contact_y/update")]
        [HttpPost]
        [ValidateModel]
        public HttpResponseMessage UpdateY([FromBody]YViewModel model)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  var contact = ContactYRepository.Update(model.ConvertToDomainModelY(), auth.GetUserId());
                  return contact.ConvertToViewModelY();
              },
              MethodBase.GetCurrentMethod());
        }
        [Route("contact_y/delete/{id:int}")]
        [HttpPost]
        public HttpResponseMessage DeleteY([FromUri]int id)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  ContactYRepository.Delete(id, auth.GetUserId());
                  return true;
              },
              MethodBase.GetCurrentMethod());
        }
        [Route("contact_x/delete/{id:int}")]
        [HttpPost]
        public HttpResponseMessage DeleteX([FromUri]int id)
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  ContactXRepository.Delete(id);
                  return true;
              },
              MethodBase.GetCurrentMethod());
        }
    }
}
