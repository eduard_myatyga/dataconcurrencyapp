﻿using System;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using DataConcurrencyApp.Api.Controllers.APIs.Base;
using DataConcurrencyApp.Api.Filters;
using DataConcurrencyApp.Api.Models;
using DataConcurrencyApp.Api.Utils;
using DataConcurrencyApp.DAL.Utils;
using DataConcurrencyApp.Models.Models;

namespace DataConcurrencyApp.Api.Controllers.APIs
{
    [RoutePrefix("api/Account")]
    public class AccountController : BaseApiController
    {
      
        [Route("Login")]
        [ValidateModel]
        public HttpResponseMessage Login([FromBody]LoginViewModel model)
        {
            try
            {
                var auth = new Authorizer(Request);
                if(auth.IsAuthorized())
                    return Request.CreateResponse(HttpStatusCode.NotModified);

                var toDomain = new User() {Login = model.Login, Password = model.Password};
                var user = UserRepository.RegisterOrSignIn(toDomain, false);
                if (user == null)
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);

                var token = SessionManager.CreateSession(user.Id, true);
                var response = Request.CreateResponse(HttpStatusCode.OK, new { Login = user.Login, Token = token });
                response.Headers.Add("sTok", token);
                return response;
            }
            catch (CustomException e)
            {
                var error = new BadRequestResponse();
                error.ModelState.Add(e.GetCode(), new string[] { e.Message });
                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            
           
        }
        [Route("Registration")]
        [ValidateModel]
        public HttpResponseMessage Registration([FromBody]RegistrationViewModel model)
        {
            try
            {
                var toDomain = new User() { Login = model.Login, Password = model.Password };
                var user = UserRepository.RegisterOrSignIn(toDomain, true);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (CustomException e)
            {
                var error = new BadRequestResponse();
                error.ModelState.Add(e.GetCode(), new string[] { e.Message });
                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
            catch (Exception  ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        [Route("Logout")]
        [HttpPost]
        public HttpResponseMessage Logout()
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  return SessionManager.Logout(auth.GetSessionToken()) ? true : false;
              },
              MethodBase.GetCurrentMethod());
        }
        [Route("active")]
        [HttpGet]
        public HttpResponseMessage CheckActive()
        {
            return PerformReturnRequest(Request,
              delegate(Authorizer auth)
              {
                  return auth.IsAuthorized();
              },
              MethodBase.GetCurrentMethod());
        }
    }
}
