﻿using DataConcurrencyApp.Api.Models;
using DataConcurrencyApp.DAL.Utils;
using DataConcurrencyApp.Models.Models;

namespace DataConcurrencyApp.Api.Utils
{
    public static class Mapper
    {
        public static ContactModelX ConvertToDomainModelX(this XViewModel viewModel)
        {
            if (viewModel == null)
                throw new CustomException("Model X is  empty");
            var domain = new ContactModelX();
            domain.Id = viewModel.Id;
            domain.FirstName = viewModel.FirstName;
            domain.LastName = viewModel.LastName;
            domain.Age = viewModel.Age;
            domain.Address = viewModel.Address;
            domain.Version = viewModel.Version;
            return domain;
        }
        public static XViewModel ConvertToViewModelX(this ContactModelX domain)
        {
            if (domain == null)
                throw new CustomException("Model X is  empty");
            var viewModel = new XViewModel();
            viewModel.Id = domain.Id;
            viewModel.FirstName = domain.FirstName;
            viewModel.LastName = domain.LastName;
            viewModel.Age = domain.Age;
            viewModel.Address = domain.Address;
            viewModel.Version = domain.Version;
            return viewModel;
        }
        public static YViewModel ConvertToViewModelY(this ContactModelY domain)
        {
            if (domain == null)
                throw new CustomException("Model X is  empty");
            var viewModel = new YViewModel();
            viewModel.Id = domain.Id;
            viewModel.FirstName = domain.FirstName;
            viewModel.LastName = domain.LastName;
            viewModel.Age = domain.Age;
            viewModel.Address = domain.Address;
            viewModel.IsLocked = domain.IsLocked;
            viewModel.LockedByLogin = domain.LockedByLogin;
            return viewModel;
        }

        public static ContactModelY ConvertToDomainModelY(this YViewModel viewModel)
        {
            if (viewModel == null)
                throw new CustomException("Model X is  empty");
            var domain = new ContactModelY();
            domain.Id = viewModel.Id;
            domain.FirstName = viewModel.FirstName;
            domain.LastName = viewModel.LastName;
            domain.Age = viewModel.Age;
            domain.Address = viewModel.Address;
            return domain;
        }
    }
}