﻿using System.Linq;
using System.Net;
using System.Net.Http;

namespace DataConcurrencyApp.Api.Utils
{
    public class Authorizer
    {
        private readonly bool _isAuthorized;
        private HttpStatusCode _httpErrorCode = HttpStatusCode.OK;
        private string _sTok;
        private int _userId;

        public Authorizer(HttpRequestMessage request, string token = null)
        {
            _isAuthorized = Authorize(request, token);
        }

        public HttpStatusCode GetHttpErrorCode()
        {
            return _httpErrorCode;
        }

        public int GetUserId()
        {
            return _userId;
        }

        public bool IsAuthorized()
        {
            return _isAuthorized;
        }

        public string GetSessionToken()
        {
            return _sTok;
        }

        private bool Authorize(HttpRequestMessage request, string token)
        {

            if ((token == null || token.Length == 0) && !request.Headers.Contains("sTok"))
            {
                _httpErrorCode = HttpStatusCode.BadRequest;
                return false;
            }

            _sTok = (token == null || token.Length == 0) ? request.Headers.GetValues("sTok").First() : token;

            _userId = SessionManager.GetUserId(_sTok);
            if (_userId == 0)
            {
                _httpErrorCode = HttpStatusCode.Unauthorized;
                return false;
            }

            return true;
        }
    }
}