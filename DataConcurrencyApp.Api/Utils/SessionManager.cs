﻿using System;
using System.Collections;
using System.Linq;

namespace DataConcurrencyApp.Api.Utils
{
    public class SessionManager
    {
        // that checks for invalid sessions and removes them from memory

        static readonly Hashtable mapSessionsCache = new Hashtable();
        static readonly Hashtable mySyncdHT = Hashtable.Synchronized(mapSessionsCache);

        static readonly Hashtable mapFeatured = new Hashtable();

        public static string CreateSession(int UserId, bool rememberMe = false)
        {
            var token = TryGetUserTokenById(UserId);
            if (!string.IsNullOrEmpty(token)) return token;
            SessionInfo newSession = new SessionInfo(UserId, rememberMe);
            mySyncdHT.Add(newSession.GetSessionId(), newSession);

            return newSession.GetSessionId();
        }

        public static int GetUserId(string sSessionId)
        {
            if (mapSessionsCache.Contains(sSessionId))
            {
                var info = mySyncdHT[sSessionId] as SessionInfo;
                if (info != null && info.IsValid())
                {
                    info.UpdateAccess();
                    return info.GetUserId();
                }

                mySyncdHT.Remove(sSessionId);
            }

            return 0;
        }

        public static bool Logout(string sessionToken)
        {
            if (mapSessionsCache.Contains(sessionToken))
            {
                mySyncdHT.Remove(sessionToken);
                return true;
            }

            return false;
        }

        public static string TryGetUserTokenById(int userId)
        {
            string token = String.Empty;
            var dictionatySessionsStore = mapSessionsCache.Cast<DictionaryEntry>().ToDictionary(x => (string)x.Key, x => (SessionInfo)x.Value);
            foreach (var t in dictionatySessionsStore)
            {
                if (t.Value.GetUserId() == userId) { token = t.Key; break; }
                    
            }
            return token;
        }
    }

    public class SessionInfo
    {
        int _nUserId = 0;
        private bool _rememberMe;
        string _sessionId = Guid.NewGuid().ToString();

        DateTime _CreateionTime = DateTime.Now;
        DateTime _LastAccessTime = DateTime.Now;

        private const int SESSION_TIMEOUT = 120; // in mins

        public SessionInfo(int nUserId, bool rememberMe)
        {
            _nUserId = nUserId;
            _rememberMe = rememberMe;
        }

        public void UpdateAccess()
        {
            _LastAccessTime = DateTime.Now;
        }

        public DateTime GetLastAccess()
        {
            return _LastAccessTime;
        }

    
        public bool IsValid()
        {
            if (_rememberMe) return true; //check limit for rememberme user
            TimeSpan diff = DateTime.Now - _LastAccessTime;
            if (diff.Days > 0 || diff.Hours > 0 || diff.Minutes > SESSION_TIMEOUT)
                return false;

            return true;
        }

        public string GetSessionId()
        {
            return _sessionId;
        }

        public int GetUserId()
        {
            return _nUserId;
        }
    }
}