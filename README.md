# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

 * This is Test project for data concurrency: optimistic and pessimistic. 
 * Baskend: ASP.NET MVC + WEB.API without  any ORM only native ADO.NET. Created by Visual Studio 2013 For Web, template: default base web project template.
 * Client: html+js+jquery without any frameworks.
 * Entity X -  optimisti. 
 * Entity Y - pessimistic.

### Initialize configuration ###

* Download a project
* Restore missing nuget packeges. (For Web api) 
* Database configuration : Execute sql file in folder ../Root folder/DataConcurrencyApp.DAL/DbScripts/CreateAll.sql
* Configurate connection string: in folder ../Root folder/DataConcurrencyApp.API/web.config in section <connectionStrings>.