﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace DataConcurrencyApp.Common.Helpers
{
    public class DbHelper
    {
        #region Helper functions
        public static bool HasColumn(IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
        public static void AddParameter(IDbCommand cmd, string paramName, SqlDbType type, object value)
        {
            var param = new SqlParameter(string.Format("@{0}", paramName), type);
            param.Value = value;
            cmd.Parameters.Add(param);
        }
        public static string GetString(IDataReader dr, string fieldName)
        {
            return GetString(dr, fieldName, string.Empty);
        }

        public static string GetString(IDataReader dr, string fieldName, string defaultValue)
        {

            return GetString(dr, dr.GetOrdinal(fieldName), defaultValue);
        }

        public static string GetString(IDataReader dr, int columnIndex, string defaultValue)
        {
            return dr.IsDBNull(columnIndex) ? defaultValue : dr.GetString(columnIndex);
        }

        public static string GetString(IDataReader dr, int columnIndex)
        {
            return dr.IsDBNull(columnIndex) ? string.Empty : dr.GetString(columnIndex);
        }

        public static byte GetByte(IDataReader dr, int columnIndex)
        {
            return GetByte(dr, columnIndex, 0);
        }

        public static byte GetByte(IDataReader dr, string fieldName)
        {
            return GetByte(dr, dr.GetOrdinal(fieldName), 0);
        }

        public static byte GetByte(IDataReader dr, string fieldName, byte defaultValue)
        {
            return GetByte(dr, dr.GetOrdinal(fieldName), defaultValue);
        }

        public static byte GetByte(IDataReader dr, int columnIndex, byte defaultValue)
        {
            return dr.IsDBNull(columnIndex) ? defaultValue : dr.GetByte(columnIndex);
        }

        public static int GetInt(IDataReader dr, string fieldName)
        {
            return GetInt(dr, fieldName, 0);
        }

        public static int GetInt(IDataReader dr, string fieldName, int defaultValue)
        {
            return GetInt(dr, dr.GetOrdinal(fieldName), defaultValue);
        }

        public static int GetInt(IDataReader dr, int columnIndex, int defaultValue)
        {
            return dr.IsDBNull(columnIndex) ? defaultValue : dr.GetInt32(columnIndex);
        }

        public static short GetShort(IDataReader dr, string fieldName)
        {
            int columnIndex = dr.GetOrdinal(fieldName);
            return dr.IsDBNull(columnIndex) ? (short)0 : dr.GetInt16(columnIndex);
        }

        public static short GetShort(IDataReader dr, int columnIndex)
        {
            return dr.IsDBNull(columnIndex) ? (short)0 : dr.GetInt16(columnIndex);
        }

        public static long GetBigint(IDataReader dr, string fieldName)
        {
            return GetBigint(dr, fieldName, 0);
        }

        public static double GetDouble(IDataReader dr, int columnIndex)
        {
            return dr.IsDBNull(columnIndex) ? 0 : dr.GetDouble(columnIndex);
        }

        public static long GetBigint(IDataReader dr, string fieldName, long defaultValue)
        {
            return GetBigint(dr, dr.GetOrdinal(fieldName), defaultValue);
        }

        public static long GetBigint(IDataReader dr, int columnIndex, long defaultValue)
        {
            return dr.IsDBNull(columnIndex) ? defaultValue : dr.GetInt64(columnIndex);
        }

        public static byte[] GetBytes(IDataReader dr, string fieldName)
        {
            SqlDataReader reader = dr as SqlDataReader;
            if (reader != null)
            {
                int columnIndex = dr.GetOrdinal(fieldName);
                return dr.IsDBNull(columnIndex) ? null : reader.GetSqlBytes(columnIndex).Value;
            }
            return null;
        }

        public static double GetDouble(IDataReader dr, string fieldName)
        {
            return GetDouble(dr, dr.GetOrdinal(fieldName), 0);
        }

        public static double GetDouble(IDataReader dr, string fieldName, double defaultValue)
        {
            return GetDouble(dr, dr.GetOrdinal(fieldName), defaultValue);
        }

        public static double GetDouble(IDataReader dr, int columnIndex, double defaultValue)
        {
             return dr.IsDBNull(columnIndex) ? defaultValue : dr.GetDouble(columnIndex);
        }

        public static bool GetBoolean(IDataReader dr, string fieldName)
        {
            return GetBoolean(dr, fieldName, false);
        }

        public static bool GetBoolean(IDataReader dr, string fieldName, bool defaultValue)
        {
            return GetBoolean(dr, dr.GetOrdinal(fieldName), defaultValue);
        }

        public static bool GetBoolean(IDataReader dr, int columnIndex, bool defaultValue)
        {
            return dr.IsDBNull(columnIndex) ? defaultValue : dr.GetBoolean(columnIndex);
        }

        public static Guid GetGuid(IDataReader dr, string fieldName)
        {
            int columnIndex = dr.GetOrdinal(fieldName);
            return dr.IsDBNull(columnIndex) ? Guid.Empty : dr.GetGuid(columnIndex);
        }

        public static DateTime GetDateTime(IDataReader dr, int columnIndex)
        {
            return GetDateTime(dr, columnIndex, DateTime.MinValue);
        }

        public static DateTime GetDateTime(IDataReader dr, string fieldName)
        {
            return GetDateTime(dr, fieldName, DateTime.MinValue);
        }

        public static DateTime GetDateTime(IDataReader dr, string fieldName, DateTime defaultValue)
        {
            return GetDateTime(dr, dr.GetOrdinal(fieldName), defaultValue);
        }

        public static DateTime GetDateTime(IDataReader dr, int columnIndex, DateTime defaultValue)
        {
            return dr.IsDBNull(columnIndex) ? defaultValue : dr.GetDateTime(columnIndex);
        }

        #endregion
    }
}