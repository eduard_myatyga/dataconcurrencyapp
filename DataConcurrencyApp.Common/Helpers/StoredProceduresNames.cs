﻿namespace DataConcurrencyApp.Common.Helpers
{
    public static class StoredProceduresNames
    {
        public const string PUsersRegistrationOrSignIn = "P_Users_Registration_OR_SignIn";
        public const string PContactsYUpdate = "P_Contacts_Y_Update";
        public const string PContactsYSelectWithLock = "P_Contacts_Y_Select_With_Lock";
        public const string PContactsXUpdate = "P_Contacts_X_Update";
        public const string PContactsYInsert = "P_Contacts_Y_Insert";
        public const string PContactsXInsert = "P_Contacts_X_Insert";
        public const string PContactsXDelete = "P_Contacts_X_Delete";
        public const string PContactsYDelete = "P_Contacts_Y_Delete";
    }
}
