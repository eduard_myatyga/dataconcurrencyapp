﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataConcurrencyApp.Common.Helpers
{
    public static class StringHelper
    {
        public static string Delimit<T>(this IEnumerable<T> input, string delimiter = ".")
        {
            IEnumerator<T> enumerator = input.GetEnumerator();

            if (enumerator.MoveNext())
            {
                StringBuilder builder = new StringBuilder();

                // start off with the first element
                builder.Append(enumerator.Current);

                // append the remaining elements separated by the delimiter
                while (enumerator.MoveNext())
                {
                    builder.Append(delimiter);
                    builder.Append(enumerator.Current);
                }

                return builder.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        public static byte[] GetBytes(this string str, char splitter = '.')
        {
            var bytes = new byte[0];
            if (string.IsNullOrWhiteSpace(str))
                return bytes;
            byte n;
            bytes = str.Split(splitter).Select(x => byte.TryParse(x, out n) ? n : (byte)0).ToArray();
            return bytes;
        }
    }
}
