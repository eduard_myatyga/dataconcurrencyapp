use master
Go

CREATE DATABASE DataConcurrencyDb
GO

use [DataConcurrencyDb]
GO

CREATE TABLE [dbo].[T_Users] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Login] [nvarchar] (50) NOT NULL,
	[Password] [nvarchar] (100) NOT NULL,
	PRIMARY KEY ([Id])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_Contacts_X] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[FirstName] [nvarchar] (50) NOT NULL,
	[LastName] [nvarchar] (50) NOT NULL,
	[Age] [tinyint] NOT NULL,
	[Address] [nvarchar] (200) NOT NULL,
	[RowVersion] [timestamp] NOT NULL
	PRIMARY KEY ([Id])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_Contacts_Y] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[FirstName] [nvarchar] (50) NOT NULL,
	[LastName] [nvarchar] (50) NOT NULL,
	[Age] [tinyint] NOT NULL,
	[Address] [nvarchar] (200) NOT NULL,
	[IsLocked] [bit] NOT NULL DEFAULT (0),
	[LockedBy] [int] NULL
	PRIMARY KEY ([Id])
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[T_Contacts_Y] ADD CONSTRAINT fk_T_Contacts_Pessimistic_T_Users
FOREIGN KEY (LockedBy)
REFERENCES [dbo].[T_Users](Id)
GO

CREATE PROCEDURE dbo.P_Contacts_X_Insert
(
	@FirstName varchar(50),
	@LastName varchar(50),
	@Address varchar(200),
	@Age tinyint
)
AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO [dbo].[T_Contacts_X](FirstName, LastName, Address, Age) VALUES (@FirstName, @LastName, @Address, @Age);

	SELECT Id, FirstName, LastName, Address, Age, RowVersion FROM [dbo].[T_Contacts_X] WHERE (Id = SCOPE_IDENTITY())
	SET NOCOUNT OFF
END
GO

CREATE PROCEDURE dbo.P_Contacts_Y_Insert
(
	@FirstName varchar(50),
	@LastName varchar(50),
	@Address varchar(200),
	@Age tinyint
)
AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO [dbo].[T_Contacts_Y](FirstName, LastName, Address, Age) VALUES (@FirstName, @LastName, @Address, @Age);

	SELECT Id, FirstName, LastName, Address, Age, IsLocked, LockedBy FROM [dbo].[T_Contacts_Y] WHERE (Id = SCOPE_IDENTITY())
	SET NOCOUNT OFF
END
GO

CREATE PROCEDURE dbo.P_Contacts_X_Update
(
	@Id int,
	@FirstName varchar(50),
	@LastName varchar(50),
	@Address varchar(200),
	@Age tinyint,
	@RowVersion timestamp
)
AS
BEGIN TRAN
	SET NOCOUNT ON
	IF NOT EXISTS (SELECT 1 FROM  [dbo].[T_Contacts_X] WHERE Id = @Id)
	BEGIN
		SELECT @Id AS Id, 1003 AS ErrorCode --[Contact] not found
		SET NOCOUNT OFF
		RETURN;
	END
	IF EXISTS (SELECT 1 FROM  [dbo].[T_Contacts_X] WHERE Id = @Id AND RowVersion = @RowVersion)
		BEGIN
			UPDATE [dbo].[T_Contacts_X] SET FirstName = @FirstName,
			LastName = @LastName,
			Address = @Address, 
			Age = @Age
			WHERE (Id = @Id);
			SELECT Id, FirstName, LastName, Address, Age, RowVersion FROM [dbo].[T_Contacts_X] WHERE (Id = @Id)
		END

	IF(@@rowcount = 0)
	BEGIN
		SELECT @Id AS Id, 1004 AS ErrorCode
	END
	SET NOCOUNT OFF
COMMIT TRAN
GO

CREATE PROCEDURE dbo.P_Contacts_Y_Select_With_Lock
(
	@ContactId int,
	@UserId int,
	@IsLocked bit = 1
)
AS
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
BEGIN TRANSACTION
	SET NOCOUNT ON
	IF(@IsLocked = 0)
		BEGIN
			UPDATE [dbo].[T_Contacts_Y] SET IsLocked = 0, LockedBy = NULL
			WHERE (Id = @ContactId) AND (LockedBy = @UserId OR LockedBy IS NULL)
		END
	ELSE
		BEGIN
			UPDATE [dbo].[T_Contacts_Y] SET IsLocked = @IsLocked, LockedBy = @UserId
			WHERE (Id = @ContactId) AND (LockedBy = @UserId OR LockedBy IS NULL)
		END
	IF @@ROWCOUNT > 0
	BEGIN
		SELECT Id, FirstName, LastName, Address, Age, IsLocked, LockedBy FROM [dbo].[T_Contacts_Y] WHERE (Id = @ContactId)
	END
	ELSE
		SELECT @ContactId AS Id, 1005 AS ErrorCode -- locked
	SET NOCOUNT OFF
COMMIT TRANSACTION
GO

CREATE PROCEDURE dbo.P_Contacts_Y_Update
(
	@Id int,
	@FirstName varchar(50),
	@LastName varchar(50),
	@Address varchar(200),
	@Age tinyint,
	@LockedBy int
)
AS
BEGIN
SET NOCOUNT ON;
	UPDATE [dbo].[T_Contacts_Y] SET FirstName = @FirstName, LastName = @LastName, Address = @Address, Age = @Age, IsLocked = 0, LockedBy = NULL 
	WHERE (Id = @Id) AND (IsLocked = 1) AND (LockedBy = @LockedBy);

	If @@ROWCOUNT > 0
	BEGIN
		SELECT Id, FirstName, LastName, Address, Age, IsLocked, LockedBy FROM [dbo].[T_Contacts_Y] WHERE (Id = @Id)
	END
SET NOCOUNT OFF
END
GO

CREATE PROCEDURE dbo.P_Users_Registration_OR_SignIn
(
	@Login varchar(50),
	@Password varchar(100),
	@IsRegistration bit
)
AS
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
BEGIN TRANSACTION 
	SET NOCOUNT ON
	DECLARE @UserId AS INT;
	IF(@IsRegistration = 1)
		BEGIN
			SELECT @UserId = Id FROM [dbo].[T_Users]  WHERE Login = @Login
			IF(@UserId IS NULL)
				BEGIN
					INSERT INTO [dbo].[T_Users]([Login], [Password]) VALUES (@Login, @Password)
					SELECT Id, Login FROM [T_Users] WHERE (Id = SCOPE_IDENTITY());
				END
			ELSE
				BEGIN
					SELECT 0 AS Id, 1006 AS ErrorCode -- User login already exist
				END
		END
	ELSE
		BEGIN
			SELECT @UserId = Id FROM [dbo].[T_Users]  WHERE Login = @Login AND Password = @Password
			IF(@UserId IS NOT NULL)
				BEGIN
					SELECT Id, Login FROM [dbo].[T_Users] WHERE (Id = @UserId);
				END
			ELSE 
				BEGIN
					SELECT 0 AS Id, 1002 AS ErrorCode --user not found
				END
		END
	SET NOCOUNT OFF
COMMIT TRANSACTION
GO

CREATE PROCEDURE dbo.P_Contacts_X_Delete
(
	@ContactId int
)
AS
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
BEGIN TRANSACTION
	SET NOCOUNT ON
	IF NOT EXISTS (SELECT 1 FROM  [dbo].[T_Contacts_X] WHERE Id = @ContactId)
	BEGIN
		SELECT 0 AS Id, 1003 AS ErrorCode --[Contact] not found
		SET NOCOUNT OFF
		RETURN;
	END
	ELSE
	BEGIN
		DELETE [dbo].[T_Contacts_X] WHERE (Id = @ContactId);
	END
	SET NOCOUNT OFF
COMMIT TRANSACTION
GO

CREATE PROCEDURE dbo.P_Contacts_Y_Delete
(
	@ContactId int,
	@UserId int
)
AS
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
BEGIN TRANSACTION
	SET NOCOUNT ON
	IF NOT EXISTS (SELECT 1 FROM  [dbo].[T_Contacts_Y] WHERE Id = @ContactId)
	BEGIN
		SELECT 0 AS Id, 1003 AS ErrorCode --[Contact] not found
		SET NOCOUNT OFF
		RETURN;
	END
	ELSE
	BEGIN
		DELETE [dbo].[T_Contacts_Y] WHERE (Id = @ContactId)
		AND ((IsLocked = 1 AND LockedBy = @UserId) OR (IsLocked = 0 AND LockedBy IS NULL));
	END
	IF @@ROWCOUNT = 0
	BEGIN
		SELECT @ContactId AS Id, 1005 AS ErrorCode -- locked
	END
	SET NOCOUNT OFF
COMMIT TRANSACTION
GO

/*Initialize*/
/*

BEGIN TRAN

	EXEC [dbo].[P_Users_Registration_OR_SignIn] @Login = N'Ed1',
		@Password = N'1234',
		@IsRegistration = 1
	GO

	EXEC dbo.P_Users_Registration_OR_SignIn 'Ed2','1234',1  
	GO

	EXEC dbo.P_Users_Registration_OR_SignIn 'Ed3','1234',1  
	GO

COMMIT TRAN
GO

BEGIN TRAN

	EXEC dbo.P_Contacts_Y_Insert 'Sary','Dondo','Cidney', 14
	GO

	EXEC dbo.P_Contacts_Y_Insert 'Fred','Sere','Kyiv', 20
	GO

	EXEC dbo.P_Contacts_Y_Insert 'Bob','Dfre','Poltava', 34
	GO

	EXEC dbo.P_Contacts_X_Insert 'Kyle','GRER','Kharkov', 50
	GO

	EXEC dbo.P_Contacts_X_Insert 'Marly','Dondo','Poltava', 21
	GO

COMMIT TRAN
GO
*/
