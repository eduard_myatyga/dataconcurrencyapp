﻿using DataConcurrencyApp.Models.Models;
using System.Collections.Generic;
namespace DataConcurrencyApp.DAL.Interfaces
{
    public interface IContactXRepository:IRepository<ContactModelX>
    {
        IEnumerable<ContactModelX> GetAll(); 
        ContactModelX GetSingle(int id);
        ContactModelX Update(ContactModelX modelToUpadate);
        ContactModelX Insert(ContactModelX modelToInsert);
        void Delete(int id);
    }
}
