﻿using DataConcurrencyApp.Models.Models;
using System.Collections.Generic;

namespace DataConcurrencyApp.DAL.Interfaces
{
    public interface IContactYRepository : IRepository<ContactModelY>
    {
        IEnumerable<ContactModelY> GetAll(); 
        ContactModelY GetSingle(int modelId);
        ContactModelY GetSingle(int modelId, int userId, bool isLock);
        ContactModelY Update(ContactModelY modelToUpadate, int userId);
        ContactModelY Insert(ContactModelY modelToInsert);
        void Delete(int id, int userId);
    }
}
