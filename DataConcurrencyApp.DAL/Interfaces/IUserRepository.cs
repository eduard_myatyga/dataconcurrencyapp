﻿using DataConcurrencyApp.Models.Models;

namespace DataConcurrencyApp.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserByLogin(string login);
        User GetUserById(int id);
        User RegisterOrSignIn(User user, bool isRegistration);
    }
}
