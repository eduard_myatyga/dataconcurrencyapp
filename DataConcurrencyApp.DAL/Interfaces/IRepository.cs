﻿using System.Collections.Generic;
using System.Data.SqlClient;
using DataConcurrencyApp.Models.Models;

namespace DataConcurrencyApp.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        IEnumerable<TEntity> ExecStoredProcedure(SqlCommand command);
        IEnumerable<TEntity> GetEntities(SqlCommand command);
        TEntity GetEntity(SqlCommand command);
        TEntity MapEntity(SqlDataReader reader);
    }
}
