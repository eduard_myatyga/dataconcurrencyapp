﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataConcurrencyApp.Common.Helpers;
using DataConcurrencyApp.DAL.Interfaces;
using DataConcurrencyApp.DAL.Utils;
using DataConcurrencyApp.Models.Models;

namespace DataConcurrencyApp.DAL.Repositories
{
    public class ContactXRepository : EntityRepository<ContactModelX>,IContactXRepository
    {
        public ContactXRepository(string connectionString) : base(connectionString)
        {
        }

        public override ContactModelX MapEntity(SqlDataReader reader)
        {
            try
            {
                ErrorQueryChecker.Check(reader);
                return new ContactModelX()
                {
                    Id = DbHelper.GetInt(reader, "Id"),
                    Address = DbHelper.GetString(reader, "Address"),
                    Age = DbHelper.GetByte(reader, "Age"),
                    FirstName = DbHelper.GetString(reader, "FirstName"),
                    LastName = DbHelper.GetString(reader, "LastName"),
                    Version = DbHelper.GetBytes(reader, "RowVersion").Delimit()
                }; 
            }
            catch (CustomException)
            {
                reader.Close();
                throw;
            }
              
        }

        public ContactModelX GetSingle(int id)
        {
            using (var command = new SqlCommand("SELECT Id, FirstName, LastName, Address, Age, RowVersion FROM [dbo].[T_Contacts_X] WHERE (Id = @Id)"))
            {
                DbHelper.AddParameter(command, "Id", SqlDbType.Int, id);
                return GetEntity(command);
            }
        }

        public ContactModelX Update(ContactModelX modelToUpadate)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PContactsXUpdate))
            {
                DbHelper.AddParameter(command, "Id", SqlDbType.Int, modelToUpadate.Id);
                DbHelper.AddParameter(command, "FirstName", SqlDbType.NVarChar, modelToUpadate.FirstName);
                DbHelper.AddParameter(command, "LastName", SqlDbType.NVarChar, modelToUpadate.LastName);
                DbHelper.AddParameter(command, "Address", SqlDbType.NVarChar, modelToUpadate.Address);
                DbHelper.AddParameter(command, "Age", SqlDbType.TinyInt, modelToUpadate.Age);
                DbHelper.AddParameter(command, "RowVersion", SqlDbType.Timestamp, modelToUpadate.RowVersion);
                return ExecStoredProcedure(command).FirstOrDefault();
            }
        }

        public ContactModelX Insert(ContactModelX modelToInsert)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PContactsXInsert))
            {
                DbHelper.AddParameter(command, "FirstName", SqlDbType.NVarChar, modelToInsert.FirstName);
                DbHelper.AddParameter(command, "LastName", SqlDbType.NVarChar, modelToInsert.LastName);
                DbHelper.AddParameter(command, "Address", SqlDbType.NVarChar, modelToInsert.Address);
                DbHelper.AddParameter(command, "Age", SqlDbType.TinyInt, modelToInsert.Age);
                return ExecStoredProcedure(command).FirstOrDefault();
            }
        }

        public IEnumerable<ContactModelX> GetAll()
        {
            using (var command = new SqlCommand("SELECT Id, FirstName, LastName, Address, Age, RowVersion FROM [dbo].[T_Contacts_X]"))
            {
                return GetEntities(command);
            }
        }


        public void Delete(int id)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PContactsXDelete))
            {
                DbHelper.AddParameter(command, "ContactId", SqlDbType.Int, id);
                ExecStoredProcedure(command);
            }
        }
    }
}
