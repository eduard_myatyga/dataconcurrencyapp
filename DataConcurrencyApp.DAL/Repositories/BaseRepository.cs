﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DataConcurrencyApp.DAL.Interfaces;
using DataConcurrencyApp.Models.Models;

namespace DataConcurrencyApp.DAL.Repositories
{
    public class EntityRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly string _connection;

        public EntityRepository(string connectionString)
        {
            _connection = (connectionString);
        }

        public virtual IEnumerable<TEntity> ExecStoredProcedure(SqlCommand command)
        {
            var list = new List<TEntity>();
            using (var connection = new SqlConnection(_connection))
            {
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            var record = MapEntity(reader);
                            if (record != null) list.Add(record);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return list;
        }

        public virtual IEnumerable<TEntity> GetEntities(SqlCommand command)
        {
            var list = new List<TEntity>();
            using (var connection = new SqlConnection(_connection))
            {
                command.Connection = connection;
                connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                            list.Add(MapEntity(reader));
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return list;
        }

        public virtual TEntity GetEntity(SqlCommand command)
        {
            TEntity record = null;
            using (var connection = new SqlConnection(_connection))
            {
                command.Connection = connection;
                connection.Open();
                try
                {
                    var reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            record = MapEntity(reader);
                            break;
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return record;
        }

        public virtual TEntity MapEntity(SqlDataReader reader)
        {
            return null;
        }
    }
}