﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DataConcurrencyApp.Common.Helpers;
using DataConcurrencyApp.DAL.Interfaces;
using DataConcurrencyApp.DAL.Utils;
using DataConcurrencyApp.Models.Models;

namespace DataConcurrencyApp.DAL.Repositories
{
    public class ContactYRepository : EntityRepository<ContactModelY>, IContactYRepository
    {
        public ContactYRepository(string connectionString)
            : base(connectionString)
        {
        }

        public override ContactModelY MapEntity(SqlDataReader reader)
        {
            try
            {
                ErrorQueryChecker.Check(reader);
                return new ContactModelY()
                {
                    Id = DbHelper.GetInt(reader, "Id"),
                    Address = DbHelper.GetString(reader, "Address"),
                    Age = DbHelper.GetByte(reader, "Age"),
                    FirstName = DbHelper.GetString(reader, "FirstName"),
                    LastName = DbHelper.GetString(reader, "LastName"),
                    IsLocked = DbHelper.GetBoolean(reader, "IsLocked"),
                    LockedBy = DbHelper.GetInt(reader, "LockedBy"),
                    LockedByLogin = DbHelper.HasColumn(reader, "LockedByLogin")
                    ? DbHelper.GetString(reader, "LockedByLogin") : String.Empty
                };
            }
            catch (CustomException)
            {
                reader.Close();
                throw;
            }

        }

        public ContactModelY GetSingle(int id)
        {
            using (var command = new SqlCommand("SELECT Id, FirstName, LastName, Address, Age, IsLocked, LockedBy FROM [dbo].[T_Contacts_Y] WHERE (Id = @Id)"))
            {
                DbHelper.AddParameter(command, "Id", SqlDbType.Int, id);
                return GetEntity(command);
            }
        }

        public ContactModelY GetSingle(int modelId, int userId, bool isLock = true)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PContactsYSelectWithLock))
            {
                DbHelper.AddParameter(command, "ContactId", SqlDbType.Int, modelId);
                DbHelper.AddParameter(command, "UserId", SqlDbType.Int, userId);
                DbHelper.AddParameter(command, "IsLocked", SqlDbType.Bit, isLock);
                return ExecStoredProcedure(command).FirstOrDefault();
            }
        }

        public ContactModelY Update(ContactModelY modelToUpadate, int userId)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PContactsYUpdate))
            {
                DbHelper.AddParameter(command, "Id", SqlDbType.Int, modelToUpadate.Id);
                DbHelper.AddParameter(command, "FirstName", SqlDbType.NVarChar, modelToUpadate.FirstName);
                DbHelper.AddParameter(command, "LastName", SqlDbType.NVarChar, modelToUpadate.LastName);
                DbHelper.AddParameter(command, "Address", SqlDbType.NVarChar, modelToUpadate.Address);
                DbHelper.AddParameter(command, "Age", SqlDbType.TinyInt, modelToUpadate.Age);
                DbHelper.AddParameter(command, "LockedBy", SqlDbType.Int, userId);
                return ExecStoredProcedure(command).FirstOrDefault();
            }
        }

        public ContactModelY Insert(ContactModelY modelToInsert)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PContactsYInsert))
            {
                DbHelper.AddParameter(command, "FirstName", SqlDbType.NVarChar, modelToInsert.FirstName);
                DbHelper.AddParameter(command, "LastName", SqlDbType.NVarChar, modelToInsert.LastName);
                DbHelper.AddParameter(command, "Address", SqlDbType.NVarChar, modelToInsert.Address);
                DbHelper.AddParameter(command, "Age", SqlDbType.TinyInt, modelToInsert.Age);
                return ExecStoredProcedure(command).FirstOrDefault();
            }
        }

        public System.Collections.Generic.IEnumerable<ContactModelY> GetAll()
        {
            var sql = @"SELECT y.Id, FirstName, LastName, Address, Age, IsLocked, LockedBy, ISNULL(u.Login,'') AS LockedByLogin
                        FROM [dbo].[T_Contacts_Y] y 
                    LEFT OUTER JOIN [dbo].[T_Users] u on u.Id = y.LockedBy";
            using (var command = new SqlCommand(sql))
            {
                return GetEntities(command);
            }
        }


        public void Delete(int id, int userId)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PContactsYDelete))
            {
                DbHelper.AddParameter(command, "ContactId", SqlDbType.Int, id);
                DbHelper.AddParameter(command, "UserId", SqlDbType.Int, userId);
                ExecStoredProcedure(command);
            }
        }
    }
}
