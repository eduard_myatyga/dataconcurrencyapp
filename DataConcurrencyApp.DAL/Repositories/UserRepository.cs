﻿using DataConcurrencyApp.Common.Helpers;
using DataConcurrencyApp.DAL.Interfaces;
using DataConcurrencyApp.Models.Models;
using System.Data;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using DataConcurrencyApp.DAL.Utils;

namespace DataConcurrencyApp.DAL.Repositories
{
    public class UserRepository : EntityRepository<User>, IUserRepository
    {
        public UserRepository(string connectionString) : base(connectionString)
        {
        }

        public override User MapEntity(SqlDataReader reader)
        {
            try
            {
                ErrorQueryChecker.Check(reader);
                var user = new User()
                {
                    Id = DbHelper.GetInt(reader, "Id"),
                    Login = DbHelper.GetString(reader, "Login", "")
                };
                return user;
            }
            catch(CustomException)
            {
                reader.Close();
                throw;
            }     
        }

        public User GetUserByLogin(string login)
        {
            using (var command = new SqlCommand("SELECT Id, Login FROM [dbo].[T_Users] WHERE Login = @login"))
            {
                command.Parameters.Add(new ObjectParameter("login", login));
                return GetEntity(command);
            }
        }

        public User GetUserById(int id)
        {
            using (var command = new SqlCommand("SELECT Id, Login FROM [dbo].[T_Users] WHERE (Id = @id)"))
            {
                command.Parameters.Add(new ObjectParameter("id", id));
                return GetEntity(command);
            }
        }

        public User RegisterOrSignIn(User newUser,bool isRegistration = false)
        {
            using (var command = new SqlCommand(StoredProceduresNames.PUsersRegistrationOrSignIn))
            {
                DbHelper.AddParameter(command, "Login", SqlDbType.NVarChar, newUser.Login);
                DbHelper.AddParameter(command, "Password", SqlDbType.NVarChar, newUser.Password);
                DbHelper.AddParameter(command, "IsRegistration", SqlDbType.Bit, isRegistration);
                return ExecStoredProcedure(command).FirstOrDefault();
            }
        }
    }
}