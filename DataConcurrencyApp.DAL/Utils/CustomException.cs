﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using DataConcurrencyApp.Common.Helpers;

namespace DataConcurrencyApp.DAL.Utils
{
    public class CustomException : Exception
    {
        protected ErrorCodes Code = ErrorCodes.UnhandledException;

        public HttpStatusCode GetCode()
        {
            var resultCode = HttpStatusCode.BadRequest;
            switch (Code)
            {
                   case ErrorCodes.ConcurrencyWasDetected: resultCode = HttpStatusCode.Conflict; break;
                   case ErrorCodes.ErrorUserNotFound:
                    resultCode = HttpStatusCode.NotFound; break;
            }
            return resultCode;
        }

        public CustomException(string message)
            : base(message)
        {
        }

        public CustomException(ErrorCodes code, string message)
            : base(message)
        {
            Code = code;
        }
    }
    public enum ErrorCodes
    {
        UnhandledException = 0,
        ErrorBadCredentials = 1001,
        ErrorUserNotFound = 1002,
        NoContentFound = 1003,
        ConcurrencyWasDetected = 1004,
        ContentLocked = 1005,
        UserLoginIsExist = 1006
    }
    public class BadRequestResponse
    {
        public BadRequestResponse()
        {
            ModelState = new Dictionary<HttpStatusCode, string[]>();
        }
        public Dictionary<HttpStatusCode, string[]> ModelState { get; private set; }
    }
    public class ErrorQueryChecker
    {
        public static void Check(SqlDataReader reader)
        {
            var exist = DbHelper.HasColumn(reader, "ErrorCode");
            if(exist)
            {
                var code = (ErrorCodes)DbHelper.GetInt(reader, "ErrorCode");
                switch(code)
                {
                    case ErrorCodes.ErrorBadCredentials: throw new CustomException(ErrorCodes.ErrorBadCredentials, "");
                    case ErrorCodes.ErrorUserNotFound: throw new CustomException(ErrorCodes.ErrorUserNotFound, "The username or password is incorrect.");
                    case ErrorCodes.NoContentFound: throw new CustomException(ErrorCodes.NoContentFound, "The content not found.");
                    case ErrorCodes.ConcurrencyWasDetected: throw new CustomException(ErrorCodes.ConcurrencyWasDetected, "Concurrency was Detected, plase resolve conflicts.");
                    case ErrorCodes.ContentLocked: throw new CustomException(ErrorCodes.ContentLocked, "The content locked by another user.");
                    case ErrorCodes.UserLoginIsExist: throw new CustomException(ErrorCodes.UserLoginIsExist, "User login already exist.");
                }
            }
        }
    }
}
