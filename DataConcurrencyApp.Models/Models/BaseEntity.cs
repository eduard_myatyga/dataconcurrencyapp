﻿namespace DataConcurrencyApp.Models.Models
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
