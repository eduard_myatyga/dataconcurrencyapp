﻿namespace DataConcurrencyApp.Models.Models
{
    public class ContactModelY:BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public bool IsLocked { get; set; }
        public int LockedBy { get; set; }
        public string LockedByLogin { get; set; }
    }
}
