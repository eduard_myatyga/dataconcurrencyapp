﻿using DataConcurrencyApp.Common.Helpers;
using Newtonsoft.Json;

namespace DataConcurrencyApp.Models.Models
{
    
    public class ContactModelX:BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte Age { get; set; }
        public string Address { get; set; }
        [JsonIgnore]
        public byte[] RowVersion {
            get { return Version.GetBytes(); }
        }
        public string Version { get; set; }
    }
}
